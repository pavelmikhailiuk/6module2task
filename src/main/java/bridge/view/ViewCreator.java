package bridge.view;

import java.awt.Frame;

public interface ViewCreator {
	void createView(Frame frame);
}
