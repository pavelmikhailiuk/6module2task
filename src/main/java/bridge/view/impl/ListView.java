package bridge.view.impl;

import java.awt.Frame;
import java.awt.List;
import java.util.ArrayList;

import bridge.view.ViewCreator;
import bridge.view.datasource.Data;

public class ListView implements ViewCreator {

	private Data data;

	public ListView(Data data) {
		this.data = data;
	}

	public void createView(Frame frame) {
		java.util.List<String> dataList = transformData();
		List list = new List(dataList.size(), true);
		for (String row : dataList) {
			list.add(row);
		}
		frame.add(list);
	}

	private java.util.List<String> transformData() {
		java.util.List<String> transformed = new ArrayList<String>();
		Object[][] dataObjects = data.getData();
		for (int i = 0; i < dataObjects.length; i++) {
			Object[] row = dataObjects[i];
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < row.length; j++) {
				sb.append(row[j]);
			}
			transformed.add(sb.toString());
		}
		return transformed;
	}
}
