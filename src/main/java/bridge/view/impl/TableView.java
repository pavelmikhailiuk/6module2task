package bridge.view.impl;

import java.awt.Frame;

import javax.swing.JTable;

import bridge.view.ViewCreator;
import bridge.view.datasource.Data;

public class TableView implements ViewCreator {

	private Data data;

	public TableView(Data data) {
		this.data = data;
	}

	public void createView(Frame frame) {
		JTable table = new JTable(data.getData(), data.getColumnNames());
		frame.add(table);
	}
}
