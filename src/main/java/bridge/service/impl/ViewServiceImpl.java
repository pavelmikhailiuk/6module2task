package bridge.service.impl;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import bridge.service.ViewService;
import bridge.view.ViewCreator;

public class ViewServiceImpl implements ViewService {

	private List<ViewCreator> creators;

	public void createView() {
		Frame frame = new Frame();
		frame.setTitle("Bridge pattern");
		frame.setSize(1376, 768);
		frame.setLayout(new FlowLayout());
		for (ViewCreator creator : creators) {
			creator.createView(frame);
		}
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
	}
}
