package bridge.main;

import bridge.service.ViewService;
import bridge.service.impl.ViewServiceImpl;

public class Runner {
	public static void main(String[] args) {
		ViewService service = new ViewServiceImpl();
		service.createView();
	}
}
